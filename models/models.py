# -*- coding: utf-8 -*-

from openerp import models, fields, api

class GoExpoShipment(models.Model):
    _name = 'go.shipment'
    name = fields.Char("Name")
    shipment_no = fields.Char("Shipment No.")
    startDate = fields.Date("Starting Date")
    supplier = fields.Many2one('res.partner', "Supplier")
    product_id = fields.Many2one('product.template', "Product")
    pur_price = fields.Float("Purchase Price")
    country_state = fields.Char("State")
    qty = fields.Integer("Quantity")
    is_ready = fields.Selection(selection=[('ready','Ready'),('need','To be cleaned')], default='need')

    customer_id = fields.Many2one('res.partner', "Customer")
    customer_country = fields.Many2one('res.country',"Country",related='customer_id.country_id')
    customer_address = fields.Char('City', related='customer_id.city')

    cont_no = fields.Char("Contract No.")
    cont_att = fields.Binary("Contract Attachment")
    form_no = fields.Char("Form No.")
    form_att = fields.Binary("Form attachment")
    cont_doc = fields.Boolean("Contract verified?")
    form_doc = fields.Boolean("Form verified?")
    specs = fields.Boolean("Specifications & Quality")
    clearance_rep = fields.Many2one('go.person',"Clearance Representative")

    containers_res = fields.Boolean("Containers Reservation")
    transport_rep = fields.Many2one('go.person', "Transportation Representative")
    policy_att = fields.Binary("Policy Attachment")
    policy_state = fields.Selection(selection=[('ready','Ready'),('need','To be Altered')], default='need')

    cer_specs = fields.Binary("Specification Certificate")
    cer_health = fields.Binary("Health Certificate")
    cer_source = fields.Binary("Source Certificate")
    cer_com = fields.Binary("Commercial Invoice")
    cer_test = fields.Binary("Test Certificate")
    cer_fill = fields.Binary("Fill Certificate")
    cer_evp = fields.Binary("Evaporation Certificate")
    cer_th = fields.Binary("Third Party Certificate")
    cer_special = fields.Binary("Special Certificate")

    sale_price = fields.Float("Sale Price")
    is_policy = fields.Boolean("Policy Ready?")
    payment_method = fields.Many2one('account.payment.term',"Payment Method")
    ship_date = fields.Date("Shipping Date")
    total_outcome = fields.Float('Outcome', compute="_calculate_total_outcome")
    due_date = fields.Date("Due Date", compute="_calculate_due_date")
    revenue = fields.Float("Revenue")
    sale_rate = fields.Float("Sale Rate")
    total_sale_price = fields.Float("Total Sale Price")
    state = fields.Selection(selection=[('open','Open'),('pending','pending'),('done','Done')], default='open')

    def _calculate_due_date(self):
        self.due_date = self.startDate + 30
        return True

    def _calculate_total_outcome(self):
        if self.product_id.outcome and self.qty:
            self.total_outcome = self.product_id.outcome * self.qty
        else:
            self.total_outcome = 0
        return True


class GoExpoProducts(models.Model):
    _inherit = 'product.template'
    region = fields.Many2one('go.region', 'Region Name')
    expectedMin = fields.Float("Expected Price (Min)")
    expectedMax = fields.Float("Expected Price (Max)")
    outcome = fields.Float("Outcome")


class GoExpoRegions(models.Model):
    _name = 'go.region'
    name = fields.Char('Region Name')


class GoExpoPersons(models.Model):
    _name = 'go.person'
    name = fields.Char("Name")
    phone = fields.Integer("Phone")
    email = fields.Char("Email")
    address = fields.Char("Address")